import Vue from 'vue'

const BASE_URL =
  process.env.NODE_ENV === 'development' ? 'http://127.0.0.1:3000' : ''

export async function post(uri, data, headers = {}) {
  const response = await fetch(BASE_URL + uri, {
    body: JSON.stringify(data),
    headers: {
      'Content-Type': 'application/json',
      ...headers
    },
    method: 'POST'
  })
  return response.json()
}

Vue.use(v => (v.prototype.$http = { post }))
