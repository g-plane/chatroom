import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import router from './router'
import store from './store'
import './fetch'
import 'roboto-fontface/css/roboto/roboto-fontface.css'
import 'material-design-icons-iconfont/dist/material-design-icons.css'

Vue.config.productionTip = false

// eslint-disable-next-line no-console
Vue.prototype.$log = message => console.info('[信息] ' + message)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
