import Vue from 'vue'
import Vuex from 'vuex'
import io from 'socket.io-client'
import createECDH from 'create-ecdh'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    logged: false,
    ecdh: createECDH('secp521r1'),
    socket: io(
      process.env.NODE_ENV === 'development' ? 'http://localhost:3000' : '/'
    ),
    drawer: true,
    record: Object.create(null),
    unread: Object.create(null)
  },
  mutations: {
    toggleLogged(state) {
      state.logged = !state.logged
    },
    addMessage(state, { from, body }) {
      state.record[from] = state.record[from] || []
      state.record[from].push(body)
    },
    addUnread(state, { from, body }) {
      state.unread[from] = state.unread[from] || []
      state.unread[from].push({ body, date: new Date() })
    },
    clearUnread(state, { from }) {
      state.unread[from] = null
    },
    toggleDrawer(state) {
      state.drawer = !state.drawer
    }
  }
})
