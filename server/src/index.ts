import * as path from 'path'
import * as fastify from 'fastify'
import * as cors from 'fastify-cors'
import * as serve from 'fastify-static'
import * as compress from 'fastify-compress'
import './env'
import * as handlers from './handlers'
import ws from './ws'

const app = fastify()
app.register(cors)
app.register(serve, {
  root: path.join(__dirname, 'public')
})
app.register(compress)

ws(app.server)

app.all('/', (_, rep) => {
  rep.sendFile('index.html')
})
app.post('/login', handlers.login)
app.post('/register', handlers.register)

const port = +(process.env.PORT || 3000)
app
  .listen(port, '0.0.0.0')
  .then(address => console.log(`Server is running at port ${address}.`))
