import { IncomingMessage } from 'http'
import * as fastify from 'fastify'
import * as bcrypt from 'bcrypt'
import User, { User as UserInstance } from '../models/user'

export default async (req: fastify.FastifyRequest<IncomingMessage>) => {
  const { username, password } = req.body

  if (!username) {
    return { code: 1, message: '用户名不能为空' }
  }
  if (!password) {
    return { code: 1, message: '密码不能为空' }
  }

  try {
    const user = (await User.findOne({
      where: {
        username
      }
    })) as UserInstance | null

    if (!user) {
      return { code: 2, message: '您还没注册' }
    }

    if (!(await bcrypt.compare(password, user.password))) {
      return { code: 3, message: '密码不对' }
    }

    return {
      code: 0,
      message: '登录成功！'
    }
  } catch (e) {
    return { code: -1, message: 'Internal error: ' + e.message }
  }
}
