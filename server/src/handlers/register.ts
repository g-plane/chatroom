import { IncomingMessage } from 'http'
import { FastifyRequest } from 'fastify'
import * as bcrypt from 'bcrypt'
import User from '../models/user'

export default async (req: FastifyRequest<IncomingMessage>) => {
  const { username, password } = req.body

  if (!username) {
    return { code: 1, message: '用户名不能为空' }
  }
  if (!password) {
    return { code: 1, message: '密码不能为空' }
  }

  try {
    const dup = await User.findOne({ where: { username } })
    if (dup) {
      return { code: 2, message: '您已经注册过了' }
    }

    await User.create({
      id: 0,
      username,
      password: await bcrypt.hash(password, 10)
    })

    return { code: 0, message: '注册已完成' }
  } catch (e) {
    return { code: -1, message: 'Internal error: ' + e.message }
  }
}
