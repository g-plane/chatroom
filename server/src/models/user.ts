import * as Sequelize from 'sequelize'
import sequelize from '../db'

const User = sequelize.define(
  'user',
  {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    username: Sequelize.STRING,
    password: Sequelize.STRING
  },
  {
    timestamps: false
  }
)

if (process.argv.includes('--sync')) {
  User.sync({ force: true })
}

export default User

export interface User {
  id: number
  username: string
  password: string
}
