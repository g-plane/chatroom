import { Server } from 'http'
import * as socketIO from 'socket.io'

type Bus = {
  [user: string]: {
    socket: socketIO.Socket
    keys: any
  }
}

export default (server: Server) => {
  const io = socketIO(server)
  const bus: Bus = Object.create(null)

  io.on('connection', (socket: socketIO.Socket & { username: string }) => {
    socket.on('enter', username => {
      bus[username] = { socket, keys: null }
      socket.username = username
      io.emit('return-list', Object.keys(bus))
      console.log(`"${username}" is entered.`)
    })

    socket.on('send-keys', keys => {
      bus[socket.username].keys = keys
    })

    socket.on(
      'query-keys',
      user => bus[user] && socket.emit('receive-keys', bus[user].keys)
    )

    socket.on('fetch-list', () => socket.emit('return-list', Object.keys(bus)))

    socket.on('send-message', ({ to, body }) => {
      bus[to].socket.emit('receive-message', {
        from: socket.username,
        body
      })
    })

    function leave() {
      delete bus[socket.username]
      io.emit('return-list', Object.keys(bus))
      console.log(`"${socket.username}" is left.`)
    }

    socket.on('leave', leave)

    socket.on('disconnect', leave)
  })
}
